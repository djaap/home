set nocompatible
filetype off

"-------------------------------------------------------------------------------
" vim-plug
"-------------------------------------------------------------------------------

call plug#begin('~/.vim/plugged')

" Looks
Plug 'morhetz/gruvbox'
Plug 'Rigellute/shades-of-purple.vim'
Plug 'kovetskiy/sxhkd-vim'
Plug 'chriskempson/base16-vim'
Plug 'dylanaraps/wal.vim'
Plug 'liuchengxu/space-vim-theme'
Plug 'cocopon/iceberg.vim'
Plug 'artanikin/vim-synthwave84'
Plug 'altercation/vim-colors-solarized'

"Plug 'vim-pandoc/vim-pandoc'
Plug 'vim-pandoc/vim-pandoc-syntax'
"Plug 'lervag/vimtex'
augroup pandoc_syntax
    au! BufNewFile,BufFilePre,BufRead *.md set filetype=markdown.pandoc
augroup END

" other
Plug 'godlygeek/tabular'
Plug 'christoomey/vim-tmux-navigator'
Plug 'qpkorr/vim-bufkill'
"Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'fatih/vim-go', {'do': ':GoUpdateBinaries'}

call plug#end()

"-------------------------------------------------------------------------------
" SETTINGS
"-------------------------------------------------------------------------------
" Leader
nmap <Space> <leader>

" Looks
set cursorline
set showcmd
set title

" Line Numbers
set norelativenumber
set number
nnoremap <leader>nn :set invnumber<CR> :set invrelativenumber<CR>

" Syntax/Colorscheme
"let g:gruvbox_contrast_dark='hard'
let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"

syntax enable
set background=dark
set termguicolors
colorscheme space_vim_theme
let &t_ZH="\e[3m"
let &t_ZR="\e[23m"
highlight Comment cterm=italic

filetype plugin indent on " indent (for vundle only?)


" Tabs/Indent
filetype indent plugin on
set tabstop=4       " The width of a TAB is set to x.
                    " Still it is a \t. It is just that
                    " Vim will interpret it to be having
                    " a width of x.

set shiftwidth=4    " Indents will have a width of x
set softtabstop=4   " Sets the number of columns for a TAB
set noexpandtab     " Expand TABs to spaces
"set list lcs=tab:\|\ "space here 

" Text Wrapping
set wrap
set textwidth=80

" Buffers
set hidden
nnoremap <leader>b :b *<C-z>

" Persistent undo
set undofile
set undodir=$HOME/.vim/undo
set undolevels=1000
set undoreload=10000

" Folds
set foldenable
set foldmethod=syntax
set foldlevel=1
set foldnestmax=1

" Enable mouse
if has('mouse')
	set mouse=a
endif

" Navigation 
set incsearch
nnoremap <CR> :noh<CR><CR>
nnoremap <leader>[ [[z<CR>kj
nnoremap <leader>] ]]z<CR>kj

" Finding Files
set path+=**
set wildmenu
set wildcharm=<C-z>
nnoremap <leader>e :e **/*<C-z><S-Tab>
nnoremap <leader>f :f <C-z><S-Tab>
nnoremap <leader>vrc :edit ~/.vimrc<CR> " Edit .vimrc in a new tab

" Save/Quit
nnoremap <leader>w :w<CR>
nnoremap <leader>q :wq<CR>

" Splits
nnoremap <leader>sv :vsplit<CR>
nnoremap <leader>sh :split<CR>

" Spell Check
nnoremap <leader>ss :set invspell<CR>

" Netrw
let g:netrw_liststyle=3

" Registers
nnoremap <leader>y "+y

" Make
nnoremap <leader>m :make<CR> :cwindow<CR>

" Misc
set scrolloff=5
nnoremap <leader>of :!zathura <cfile> &<CR><CR>
nnoremap <leader>gf "Ayyzjzjk

" Double press esc to escape in terminal mode tnoremap <Esc><Esc> <C-\><C-n>
tnoremap <Esc><Esc> <C-\><C-n>

" When in insert mode in terminal still be able to navigate out of split
tnoremap <silent> <c-h> <C-\><C-n>:TmuxNavigateLeft<cr>
tnoremap <silent> <c-j> <C-\><C-n>:TmuxNavigateDown<cr>
tnoremap <silent> <c-k> <C-\><C-n>:TmuxNavigateUp<cr>
tnoremap <silent> <c-l> <C-\><C-n>:TmuxNavigateRight<cr>
tnoremap <silent> <c-\> <C-\><C-n>:TmuxNavigatePrevious<cr>

" go mappings
nmap <silent> <leader>d  <Plug>(go-def)
nmap <silent> <leader>i  <Plug>(go-implements)
nmap <silent> <leader>gd <Plug>(go-doc-tab)
nmap <silent> <leader>x  <Plug>(go-diagnostics)
nmap <silent> <leader>r  <Plug>(go-referrers)
nmap <silent> <leader>n  <Plug>(go-rename)
nmap <silent> <leader>a  <Plug>(go-alternate-edit)
nmap <silent> <leader>tf <Plug>(go-test-func)
nmap <silent> <leader>ta <Plug>(go-test)

"-------------------------------------------------------------------------------
" PLUGIN SETTINGS
"-------------------------------------------------------------------------------

" Latex
let g:tex_flavor='latex'
let g:vimtex_view_method='zathura'
let g:vimtex_quickfix_mode=0
set conceallevel=1
let g:tex_conceal='abdmg'

" Ultisnips
let g:UltiSnipsListSnippets = "<c-l>"
let g:UltiSnipsSnippetDirectories=[$HOME.'/.vim/UltiSnips']

" Vim Wiki
let wiki_1 = {"path": '~/Documents/vimwiki', "syntax": 'default', "ext": '.wiki'}
let wiki_2 = {"path": '~/Documents/vimwiki/Spring2019/Autonoma', "syntax": 'markdown', "ext": '.md'}
let wiki_3 = {"path": '~/Documents/vimwiki/Spring2019/Programming Languages', "syntax": 'markdown', "ext": '.md'}

let g:vimwiki_list = [wiki_1] + [wiki_2] + [wiki_3]
let g:vimwiki_ext2syntax = {'.md': 'markdown', '.markdown': 'markdown', '.mdown': 'markdown', '.wiki': 'mediawiki'}

function! VimwikiLinkHandler(link)
	" Use Vim to open external files with the 'vfile:' scheme.  E.g.:
	"   1) [[vfile:~/Code/PythonProject/abc123.py]]
	"   2) [[vfile:./|Wiki Home]]
	let link = a:link
	if link =~# '^vfile:'
		let link = link[1:]
	else
		return 0
	endif
	let link_infos = vimwiki#base#resolve_link(link)
	if link_infos.filename == ''
		echomsg 'Vimwiki Error: Unable to resolve link!'
		return 0
	else
		exe 'tabnew ' . fnameescape(link_infos.filename)
		return 1
	endif
endfunction


"-------------------------------------------------------------------------------
" FILETYPE SPECIFIC SETTINGS
"-------------------------------------------------------------------------------

" lisp
autocmd FileType lisp set tabstop=2
autocmd FileType lisp set shiftwidth=2
autocmd FileType lisp set softtabstop=2
autocmd FileType lisp set expandtab

"python
autocmd FileType py set tabstop=4
autocmd FileType py set shiftwidth=4
autocmd FileType py set softtabstop=4 " Sets the number of columns for a TAB
autocmd FileType py set expandtab

"php
autocmd FileType php set tabstop=4
autocmd FileType php set shiftwidth=4
autocmd FileType php set softtabstop=4  
autocmd FileType php set noexpandtab

"md
autocmd FileType pandoc set tabstop=4
autocmd FileType pandoc set shiftwidth=4
autocmd FileType pandoc set softtabstop=4
autocmd FileType pandoc set expandtab
"md
autocmd FileType md set tabstop=4
autocmd FileType md set shiftwidth=4
autocmd FileType md set softtabstop=4   " Sets the number of columns for a TAB
autocmd FileType md set noexpandtab

"go
autocmd FileType go set tabstop=4
autocmd FileType go set shiftwidth=4
autocmd FileType go set softtabstop=4   " Sets the number of columns for a TAB
autocmd FileType go set noexpandtab

"html
autocmd FileType html set tabstop=4
autocmd FileType html set shiftwidth=4
autocmd FileType html set softtabstop=4   
autocmd FileType html set noexpandtab

let g:pandoc#folding#fdc = 0
