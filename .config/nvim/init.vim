source ~/.vimrc
set shada=!,'100,<50,s10,h,%

" share data between nvim instances (registers etc)
augroup SHADA
    autocmd!
    autocmd CursorHold,TextYankPost,FocusGained,FocusLost *
                \ if @% != "" && !&readonly | write | endif |
                \ if exists(':rshada') | rshada | wshada | endif
augroup END
